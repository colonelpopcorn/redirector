class Redirector {
  constructor(windowLocation) {
    this.windowLocation = windowLocation;
    this.DEFAULT_TIMEOUT = 3000;
  }
  async getReplacementURIMap() {
    // REPLACEME: this is where you set your definitions.
    return await fetch("/config.json")
      .then((res) => res.json())
      .catch((err) => {
        console.error(err);
        return {
          urls: {
            "plex.mydomain.com": {
              value: "media.mydomain.com",
              scheme: "https",
            },
          },
          redirectTimeout: 3000,
        };
      });
  }

  // FIXME: Why are you like this? YAGNI
  generateKeys(key) {
    const initialUrl = this.windowLocation.hostname;
    const newKeys = {};
    const defaultPrefixes = [
      "www.",
      "http://",
      "http://www.",
      "https://",
      "https://www.",
    ];
    defaultPrefixes.forEach(
      (prefix) => (newKeys[`${prefix}${initialUrl}`] = key)
    );
    newKeys[initialUrl] = key;
    return newKeys;
  }

  async detectAndRedirect() {
    document.getElementById(
      "missing-url"
    ).innerHTML = ` ${this.windowLocation.href} `;
    const lookupResult = await this.replaceURI();
    setTimeout(
      () => lookupResult.func(lookupResult.param),
      this.DEFAULT_TIMEOUT
    );
  }

  async replaceURI() {
    const redirectFunc = (replacementURI) =>
      (window.location.href = replacementURI);
    const notFoundFunc = (emptyLookupUrl) => {
      const newHtml = `<div id="notification-text">Man, we really couldn't find anything for
        <pre id="missing-url">${emptyLookupUrl}</pre> sorry about that...
      </div>`;
      document.getElementById("notification-text").outerHTML = newHtml;
      const preAnimationContainer = document.getElementById(
        "pre-animation-container"
      );
      const postAnimationContainer = document.getElementById(
        "post-animation-container"
      );
      preAnimationContainer.style = `${preAnimationContainer.style}; display: none;`;
      postAnimationContainer.style = `${postAnimationContainer.style}; display: flex;`;
    };
    const { urls, redirectTimeout } = await this.getReplacementURIMap();
    if (urls.hasOwnProperty(this.windowLocation.hostname)) {
      const { value, scheme } = urls[this.windowLocation.hostname];
      return {
        func: redirectFunc,
        param: `${scheme}://${value}`,
        redirectTimeout: redirectTimeout || this.DEFAULT_TIMEOUT,
      };
    } else {
      // TODO: Stop animation and say 'no really, we can't find it'.
      return { func: notFoundFunc, param: this.windowLocation.href };
    }
  }
}
