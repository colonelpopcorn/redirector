# Redirector
Redirects users from one url to the other using some JavaScript tomfoolery.

I made this so I could migrate from Plex to JellyFin without having to email everyone who was using Plex about the new url.

Example Env file shows you how to override some things.

Using a dependency free Node.js web framework called [urest](https://github.com/conorturner/urest).

Server is best used with traefik, but other proxy servers can certainly be used. See the docker-compose.example.yml for more information.

I plan on building out a traefik plugin as well to implement this same functionality.